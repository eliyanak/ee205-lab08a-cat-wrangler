///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file my_test.cpp
/// @version 2.0
///
/// Unit test for this lab
///
/// @author Eliya Nakamura <eliyanak@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   08_Apr_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cassert>

#include "list.hpp"
#include "node.hpp"

using namespace std;

int main() {

   Node node;  // Instantiate a node
   DoubleLinkedList aList;  // Instantiate a DoubleLinkedList
   Node* N1a = new Node;
   Node* N2a = new Node;
   Node* N3a = new Node;
   Node* N4a = new Node;
   Node* N5a = new Node;

   // test empty and size
   cout << endl << "List empty: " << boolalpha << aList.empty() << endl;
   cout << "There are currently " << aList.size() << " nodes." << endl;
  
   // test push_front
   aList.push_front(N3a);
   aList.push_front(N2a);
   aList.push_front(N1a);
   // test size
   cout << aList.size() << " nodes pushed to front: " << N1a << ", " << N2a << ", " << N3a << endl;

   // test empty
   cout << "List empty: " << boolalpha << aList.empty() << endl;

   // test get_first
   cout << "The first node in the list is " << aList.get_first() << endl;

   // test get_next
   cout << "The next node in the list is " << aList.get_next( aList.get_first() ) << endl;

   // test pop_front
   aList.pop_front();
   cout << "The first node has been popped out. There are currently " << aList.size() << " nodes in the list." << endl;
   cout << "The new first node in the list is " << aList.get_first() << endl;

   cout << endl <<  "--------------" << endl << endl; 

   // test push_back
   aList.push_back(N4a);
   aList.push_back(N5a);

   // test get_last and get_prev
   cout << "Nodes " << N4a << " and " << N5a << " have been pushed back." << endl;
   cout << "The last node is " << aList.get_last() << ". The node before it is " << aList.get_prev( aList.get_last() ) << endl;
   cout << "There are currently " << aList.size() << " nodes." << endl;
   
   // test pop_back
   cout << aList.pop_back() << " popped out from back." << endl;
   cout << aList.size() << " items in list." << endl;
   cout << "New last node: " << aList.get_last() << endl; 
   cout << endl << "--------------" << endl << endl;

   // test insert_after
   Node* N6a = new Node;
   Node* N7a = new Node;
   // insert N6 after first node
   cout << N6a << " will be inserted after the first node." << endl;
   aList.insert_after( aList.get_first(), N6a );
   cout << "Head: " << aList.get_first() << " | Node 2: " << aList.get_next( aList.get_first() ) << endl << endl;

   // ADJACENT SWAP TESTING

   /*DoubleLinkedList list;  // Instantiate a DoubleLinkedList
   Node* N1 = new Node;
   Node* N2 = new Node;
   Node* N3 = new Node;
   Node* N4 = new Node;
   Node* N5 = new Node;

   cout << endl << "Testing list with 2 nodes." << endl;
   list.push_front(N2);
   list.push_front(N1);

   // Test head/tail swap with two nodes in list
   cout << endl << "Head: " << list.get_first() << " | Tail: " << list.get_last() << endl;
   cout << list.size() << " nodes." << endl;
   list.swap(N1,N2);
   cout << "Head: " << list.get_first() << " | Tail: " << list.get_last() << endl;
   cout << list.size() << " nodes." << endl << endl;
   list.swap(N1,N2);
   cout << "Head: " << list.get_first() << " | Tail: " << list.get_last() << endl;
   cout << list.size() << " nodes." << endl << endl;

   cout << "Adding three more nodes." << endl << endl;

   list.push_back(N3);
   list.push_back(N4);
   list.push_back(N5);

   // Check if tail and adjacent node swap works
   cout << "Node 4: " << list.get_prev( list.get_last() ) << " | Tail: " << list.get_last() << endl;
   cout << list.size() << " nodes." << endl;
   list.swap(N4,N5);
   cout << "Node 4: " << list.get_prev( list.get_last() ) << " | Tail: " << list.get_last() << endl;
   cout << list.size() << " nodes." << endl << endl;
   list.swap(N4,N5);
   cout << "Node 4: " << list.get_prev( list.get_last() ) << " | Tail: " << list.get_last() << endl;
   cout << list.size() << " nodes." << endl << endl;

   // Check if head and adjacent node swap works
   cout << "Head: " << list.get_first() << " | Node 2: " << list.get_next( list.get_first() ) << endl;
   cout << list.size() << " nodes." << endl;
   list.swap(N1,N2);
   cout << "Head: " << list.get_first() << " | Node 2: " << list.get_next( list.get_first() ) << endl;
   cout << list.size() << " nodes." << endl << endl;
   list.swap(N1,N2);
   cout << "Head: " << list.get_first() << " | Node 2: " << list.get_next( list.get_first() ) << endl;
   cout << list.size() << " nodes." << endl << endl;

   // Check if general adjacent swap works
   cout << "Node 3: " << list.get_next( list.get_next( list.get_first() ) ) << " | Node 4: " << list.get_prev( list.get_last() ) << endl;
   cout << list.size() << " nodes in list." << endl;
   list.swap(N3,N4);
   cout << "Node 3: " << list.get_next( list.get_next( list.get_first() ) ) << " | Node 4: " << list.get_prev( list.get_last() ) << endl;
   cout << list.size() << " nodes in list." << endl << endl;
   list.swap(N3,N4);
   cout << "Node 3: " << list.get_next( list.get_next( list.get_first() ) ) << " | Node 4: " << list.get_prev( list.get_last() ) << endl;
   cout << list.size() << " nodes in list." << endl << endl;
   cout << "--------------------------------" << endl << endl;

   
   // NON-ADJACENT SWAP TESTING
   
   // Test head and tail swap
   cout << "Head: " << list.get_first() << " | Tail: " << list.get_last() << endl;
   cout << list.size() << " nodes in list." << endl;
   list.swap(N1,N5);
   cout << "Head: " << list.get_first() << " | Tail: " << list.get_last() <<
endl;
   cout << list.size() << " nodes in list." << endl << endl;
   list.swap(N1,N5);
   cout << "Head: " << list.get_first() << " | Tail: " << list.get_last() <<
endl;
   cout << list.size() << " nodes in list." << endl << endl;

   // test head and non tail node swap
   cout << "Head: " << list.get_first() << " | Node 4: " << list.get_prev( list.get_last() ) << endl;
   cout << list.size() << " nodes in list." << endl;
   list.swap(N1,N4);
   cout << "Head: " << list.get_first() << " | Node 4: " << list.get_prev( list.get_last() ) << endl;
   cout << list.size() << " nodes in list." << endl << endl;
   list.swap(N1,N4);
   cout << "Head: " << list.get_first() << " | Node 4: " << list.get_prev( list.get_last() ) << endl;
   cout << list.size() << " nodes in list." << endl << endl;

   // test tail and non head node swap
   cout << "Node 2: " << list.get_next( list.get_first() ) << " | Tail: " << list.get_last() << endl;
   cout << list.size() << " nodes in list." << endl;
   list.swap(N2,N5);
   cout << "Node 2: " << list.get_next( list.get_first() ) << " | Tail: " << list.get_last() << endl;
   cout << list.size() << " nodes in list." << endl << endl;
   list.swap(N5,N2);
   cout << "Node 2: " << list.get_next( list.get_first() ) << " | Tail: " << list.get_last() << endl;
   cout << list.size() << " nodes in list." << endl << endl;

   // test general non adjacent swap
   cout << "Node 2: " << list.get_next( list.get_first() ) << " | Node 4: " << list.get_prev( list.get_last() ) << endl;
   cout << list.size() << " nodes in list." << endl;
   list.swap(N2,N4);
   cout << "Node 2: " << list.get_next( list.get_first() ) << " | Node 4: " << list.get_prev( list.get_last() ) << endl;
   cout << list.size() << " nodes in list." << endl << endl;
   list.swap(N2,N4);
   cout << "Node 2: " << list.get_next( list.get_first() ) << " | Node 4: " << list.get_prev( list.get_last() ) << endl;
   cout << list.size() << " nodes in list." << endl << endl;
*/
}
