///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.cpp
/// @version 2.0
///
/// Exports data for lists
///
/// @author Eliya Nakamura <eliyanak@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   08_Apr_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cassert>
#include "list.hpp"

using namespace std;

const bool DoubleLinkedList::empty() const {
   // if head points to null, the list is empty
   if (head == nullptr)
      return true;
   else
      return false;
}

void DoubleLinkedList::push_front( Node* newNode ) {
   if( newNode == nullptr )
      return;
   
   if( head != nullptr ) {
      newNode->next = head;
      newNode->prev = nullptr;
      head->prev = newNode;
      head = newNode;
   } else {
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
   }
}

Node* DoubleLinkedList::pop_front() {
   // if list is already empty, return null
   if ( head == nullptr ) {
      return nullptr;
   }
   // if one node is in the list
   else if ( head == tail ) {
      Node* tmp = head;
      head = nullptr;
      tail = nullptr;
      return tmp;
   } 
   // general case
   else {
      Node* tmp = head;
      head = head->next;
      head->prev = nullptr;
      return tmp;
   }
}

Node* DoubleLinkedList::get_first() const {
   if (head == nullptr) {
      return nullptr;
   } else {
      return head;
   }
}

Node* DoubleLinkedList::get_next( const Node* currentNode ) const {
   if( currentNode -> next == nullptr ) {
      return nullptr;
   } else {
      return currentNode -> next;
   }
}

void DoubleLinkedList::push_back( Node* newNode ) {
   if( newNode == nullptr )
      return;

   // general case (when list is not empty) 
   if( tail != nullptr ) {
      newNode->next = nullptr;
      newNode->prev = tail;
      tail->next = newNode;
      tail = newNode;
   } else { // when the list is empty
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
   }
}

Node* DoubleLinkedList::pop_back() {
   // if list is empty
   if( head == nullptr ) {
      return nullptr;
   }
   // if there is one node in the list
   else if ( head == tail ) {
      Node* tmp = tail;
      head = nullptr;
      tail = nullptr;
      return tmp;
   }
   // general case
   else {
      Node* tmp = tail;
      tail = tail->prev;
      tail->next = nullptr;
      return tmp;
   }
}

Node* DoubleLinkedList::get_last() const {
   // if list is empty
   if( head == nullptr ) {
      return nullptr;
   }
   // otherwise return the tail
   else {
      return tail;
   }
}

Node* DoubleLinkedList::get_prev( const Node* currentNode ) const {
   if( currentNode->prev == nullptr ) {
      return nullptr;
   } else {
      return currentNode->prev;
   }
}

const bool DoubleLinkedList::isIn( Node* aNode ) const {
   Node* currentNode = head;

   while( currentNode != nullptr ) {
      if( aNode == currentNode )
         return true;
      currentNode = currentNode->next;
   }
   return false; // never found aNode in the list
}

void DoubleLinkedList::insert_after( Node* currentNode, Node* newNode ) {
   // if list is empty, push newNode to front
   if( currentNode == nullptr && head == nullptr ) {
      cout << "EMPTY LIST DETECTED, NODE PUSHED TO FRONT" << endl;
      push_front( newNode );
      return;
   }
   // end function if newNode is empty (error)
   else if( newNode == nullptr ) {
      cout << "ERROR: NEW NODE EMPTY" << endl;
      return;
   }
   // end function if currentNode is not zero but list is empty (error)
   else if( currentNode != nullptr && head == nullptr ) {
      cout << "ERROR: CURRENTNODE NOT ZERO BUT LIST EMPTY" << endl;
      return;
   }
   // end function if currentNode is empty but list is not empty (error)
   else if( currentNode == nullptr && head != nullptr ) {
      cout << "ERROR: CURRENTNODE EMPTY BUT LIST NOT EMPTY" << endl;
      return;
   }
   // end function if newNode is not in list
   else if( isIn( newNode ) ) {
      cout << "ERROR: NODE IS ALREADY IN LIST" << endl;
      return;
   }
   // general case
   else if( tail != currentNode ) {
      cout << "NODE INSERTED AFTER GIVEN NODE" << endl;
      newNode->next = currentNode->next;
      currentNode->next = newNode;
      newNode->prev = currentNode;
      newNode->next->prev = newNode;
      return;
   } 
   // if currentNode is at end of list
   else {
      cout << "NODE INSERTED AT END OF LIST (AFTER TAIL)" << endl;
      push_back( newNode );
      return;
   }
}

void insert_before( Node* currentNode, Node* newNode ) {
   
}

void DoubleLinkedList::swap( Node* node1, Node* node2 ) {
   // if either of the nodes are empty
   if ( node1 == nullptr ||  node2 == nullptr ) {
      //cout << "EMPTY NODE DETECTED" << endl;
      return;
   } 
   // else if both nodes are the same
   else if ( node1 == node2 ) {
      //cout << "NODES ARE EQUIVALENT" << endl;
      return;
   }
   // else if node1 is directly before node2
   else if ( (node1->next == node2) && (node2->prev == node1) ) {
      //cout << "NODE1-NODE2 SWAPPED TO NODE2-NODE1" << endl;
      // if node1 head and node2 tail (list of 2)
      if ( (node1->prev == nullptr) && (node2->next == nullptr) ) {
         //cout << "NODE1 HEAD, NODE2 TAIL" << endl;
         node1->next = nullptr;
         node2->prev = nullptr;
         node1->prev = node2;
         node2->next = node1;
         head = node2;
         tail = node1;
         return;
      }
      // check if node2 is tail
      else if ( node2->next == nullptr ) {
         //cout << "NODE2 IS TAIL" << endl;
         node1->next = nullptr;
         node2->prev = node1->prev;
         node1->prev = node2;
         node2->next = node1;
         node2->prev->next = node2;
         tail = node1;
         return;
      } 
      // check if node1 is head
      else if ( node1->prev == nullptr ) {
         //cout << "NODE1 IS HEAD" << endl;
         node2->prev = nullptr;
         node1->next = node2->next;
         node2->next = node1;
         node1->prev = node2;
         node2->next->prev = node2;
         head = node2;
         return;
      } 
      // if neither head nor tail
      else {
         //cout << "REGULAR ADJACENT NODE SWAP" << endl;
         node2->prev = node1->prev;
         node1->next = node2->next;
         node1->prev = node2;
         node2->next = node1;
         node2->prev->next = node2;
         node1->next->prev = node1;
         return;
      }
   }
   // else if node1 is directly after node2
   else if ( (node1->prev == node2) && (node2->next == node1) ) {
      //cout << "NODE2-NODE1 SWAPPED TO NODE1-NODE2" << endl; 
      // if node2 head and node1 tail (list of 2)
      if ( (node2->prev == nullptr) && (node1->next == nullptr) ) {
         //cout << "NODE2 HEAD, NODE1 TAIL" << endl;
         node2->next = nullptr;
         node1->prev = nullptr;
         node2->prev = node1;
         node1->next = node2;
         head = node1;
         tail = node2;
         return;
      }
      // check if node1 is tail
      else if ( node1->next == nullptr ) {
         //cout << "NODE1 IS TAIL" << endl;
         node2->next = nullptr;
         node1->prev = node2->prev;
         node2->prev = node1;
         node1->next = node2;
         node1->prev->next = node1;
         tail = node2;
         return;
      }
      // check if node2 is head
      else if ( node2->prev == nullptr ) {
         //cout << "NODE2 IS HEAD" << endl;
         node1->prev = nullptr;
         node2->next = node1->next;
         node1->next = node2;
         node2->prev = node1;
         node1->next->prev = node1;
         head = node1;
         return;
      }
      // if neither head nor tail
      else {
         //cout << "REGULAR ADJACENT NODE SWAP" << endl;
         node1->prev = node2->prev;
         node2->next = node1->next;
         node2->prev = node1;
         node1->next = node2;
         node1->prev->next = node1;
         node2->next->prev = node2;
         return;
      }
   }
   // non adjacent node swap
   else {
      //cout << "NODE1 AND NODE2 ARE NON-ADJACENT" << endl;
      // if node1 is head and node2 is tail
      if( (node1->prev == nullptr) && (node2->next == nullptr) ) {
         //cout << "NODE1 HEAD, NODE2 TAIL" << endl;
         Node* tmp = new Node;
         tmp->next = node1->next;
         tmp->prev = node2->prev;
         node1->next = nullptr;
         node2->prev = nullptr;
         node1->prev = tmp->prev;
         node2->next = tmp->next;
         node1->prev->next = node1;
         node2->next->prev = node2;
         tail = node1;
         head = node2;
         return;
      }
      // if node1 is tail and node2 is head
      else if ( (node2->prev == nullptr) && (node1->next == nullptr) ) {
         //cout << "NODE1 TAIL, NODE2 HEAD" << endl;
         Node* tmp = new Node;
         tmp->next = node2->next;
         tmp->prev = node1->prev;
         node2->next = nullptr;
         node1->prev = nullptr;
         node2->prev = tmp->prev;
         node1->next = tmp->next;
         node2->prev->next = node2;
         node1->next->prev = node1;
         tail = node2;
         head = node1;
         return;
      }
      // if node1 is head and node2 is normal node
      else if ( (node1->prev == nullptr) && (node2->next != nullptr) ) {
         //cout << "NODE1 HEAD, NODE2 NORMAL" << endl;
         Node* tmp = new Node;
         tmp->prev = node2->prev;
         tmp->next = node1->next;
         node1->prev = tmp->prev;
         node1->next = node2->next;
         node2->prev = nullptr;
         node2->next = tmp->next;
         node1->next->prev = node1;
         node1->prev->next = node1;
         node2->next->prev = node2;
         head = node2;
         return;
      }
      // if node1 is normal and node2 is head
      else if ( (node2->prev == nullptr) && (node1->next != nullptr) ) {
         //cout << "NODE1 NORMAL, NODE2 HEAD" << endl;
         Node* tmp = new Node;
         tmp->prev = node1->prev;
         tmp->next = node2->next;
         node2->prev = tmp->prev;
         node2->next = node1->next;
         node1->prev = nullptr;
         node1->next = tmp->next;
         node2->next->prev = node2;
         node2->prev->next = node2;
         node1->next->prev = node1;
         head = node1;
         return;
      }
      // if node1 is tail and node2 is normal
      else if ( (node1->next == nullptr) && (node2->prev != nullptr) ) {
         //cout << "NODE1 TAIL, NODE2 NORMAL" << endl;
         Node* tmp = new Node;
         tmp->prev = node1->prev;
         tmp->next = node2->next;
         node1->prev = node2->prev;
         node1->next = tmp->next;
         node2->prev = tmp->prev;
         node2->next = nullptr;
         node1->prev->next = node1;
         node1->next->prev = node1;
         node2->prev->next = node2;
         tail = node2;
         return;
      }
      // if node1 is normal and node2 is tail
      else if ( (node2->next == nullptr) && (node1->prev != nullptr) ) {
         //cout << "NODE1 NORMAL, NODE2 TAIL" << endl;
         Node* tmp = new Node;
         tmp->prev = node2->prev;
         tmp->next = node1->next;
         node2->prev = node1->prev;
         node2->next = tmp->next;
         node1->prev = tmp->prev;
         node1->next = nullptr;
         node2->prev->next = node2;
         node2->next->prev = node2;
         node1->prev->next = node1;
         tail = node1;
         return;
      }
      // general non adjacent swap (node1/node2 not head or tail)
      else {
         //cout << "GENERAL NON ADJACENT SWAP" << endl;
         Node* tmp = new Node;
         tmp->prev = node1->prev;
         tmp->next = node1->next;
         node1->prev = node2->prev;
         node1->next = node2->next;
         node2->prev = tmp->prev;
         node2->next = tmp->next;
         node1->prev->next = node1;
         node1->next->prev = node1;
         node2->prev->next = node2;
         node2->next->prev = node2;
         return;
      }
   }
}

const bool DoubleLinkedList::isSorted() const {
   DoubleLinkedList list;
   if( list.size() <= 1 ) 
      return true;

   for( Node* i = head ; i->next != nullptr ; i = i->next ) {
      if( *i > *i->next ) 
         return false;
   }

   return true;
}

void DoubleLinkedList::insertionSort() {
   DoubleLinkedList list;
   if( list.size() <= 1 )
      return;

   for( Node* i = head ; i->next != nullptr ; i = i->next ) {
      Node* minNode = i;

      for( Node* j = i->next ; j != nullptr ; j = j->next ) {
         if( *minNode > *j ) 
            minNode = j;
      }

      swap( i, minNode );
      i = minNode;
   }
}
